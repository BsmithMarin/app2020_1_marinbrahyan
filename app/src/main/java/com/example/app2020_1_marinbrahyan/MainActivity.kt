package com.example.app2020_1_marinbrahyan

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.FragmentTransaction
import com.example.app2020_1_marinbrahyan.fragments.PrimerFragment
import kotlin.math.log

class MainActivity : AppCompatActivity(),View.OnClickListener {

    var tvSaludo:TextView?=null
    var btnBoton1:Button?=null
    var btnBoton2:Button?=null
    var primerFragment:PrimerFragment= PrimerFragment()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        tvSaludo=this.findViewById(R.id.tvSaludo)
        tvSaludo!!.text="ADios"
        btnBoton1=this.findViewById(R.id.boton1)
        btnBoton2=this.findViewById(R.id.boton2)
        btnBoton1?.setOnClickListener(this)
        btnBoton2?.setOnClickListener(this)

        var transaction:FragmentTransaction=this.supportFragmentManager.beginTransaction()
        transaction.replace(R.id.Root,primerFragment!!)
        transaction.commit()



    }
    override fun onResume() {
        super.onResume()
        Log.v("Mainactivity","Entrado en resume")
    }


    override fun onClick(v: View?) {
        if(v==btnBoton1){
            tvSaludo?.text="Me has pinchado broh"
        }else{
            tvSaludo?.text="Te dije que no me pincharas broh"
        }
    }



}